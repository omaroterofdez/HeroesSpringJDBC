package es.plexus.cedei.heroes.controllers;

import es.plexus.cedei.heroes.model.Personaje;
import es.plexus.cedei.heroes.services.PersonajeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class HeroesController {

  private PersonajeService personajeService;


  @Autowired(required = true)
  @Qualifier(value = "personajeService")
  public void setPersonajeService(PersonajeService ps){
    this.personajeService = ps;
  }



  @RequestMapping(value = "/index")
  public String index() {
    return "index";
  }



  @RequestMapping(value = "/heroe", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String crearHeroe() {
    return "crearHeroe";
  }


  @RequestMapping(value = "/heroe", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String postHeroe(@ModelAttribute("personaje") Personaje p) {

    this.personajeService.addPersonaje(p);
    return "redirect:/heroes";
  }


  @RequestMapping(value = "/heroe/{id}", method = RequestMethod.GET,  produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<Personaje> obtenerHeroe(@PathVariable("id") Long id) {
    Personaje result = this.personajeService.getPersonaje(id);

    Personaje p =new Personaje(result.getId(), result.getNombre(), result.getTipo(), result.getBio());

    return new ResponseEntity<>(p, HttpStatus.OK);
  }

  @RequestMapping(value = "/heroe/{id}", method = RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String actualizarHeroe(Model model, @PathVariable("id") Long id) {
    Personaje p = this.personajeService.getPersonaje(id);
    model.addAttribute("personaje", p);
    p.setNombre("prueba");
    p.setBio("bioPrueba");
    this.personajeService.updatePersonaje(p);
    return "heroe";
  }

  @RequestMapping(value = "/heroes", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<List<Personaje>> obtenerHeroes() {
    ArrayList<Personaje> listaH = new ArrayList<>();
    for (Personaje heroe: this.personajeService.listaPersonajes()) {
      listaH.add(new Personaje(heroe.getId(), heroe.getNombre(),heroe.getTipo(),heroe.getBio()));
    }
    return new ResponseEntity<>(listaH, HttpStatus.OK);
  }


  @RequestMapping(value = "/heroe/{id}", method = RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String eliminarHeroe(@PathVariable("id") Long id) {
    this.personajeService.removePersonaje(id);
    return "listaPersonajes";
  }

}