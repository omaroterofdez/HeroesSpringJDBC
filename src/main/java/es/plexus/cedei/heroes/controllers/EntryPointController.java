package es.plexus.cedei.heroes.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * Created by cedei06 on 20/07/2017.
 */
@Controller
public class EntryPointController {
  private static final Logger log = Logger.getLogger(EntryPointController.class);

  @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
  public String showLogin() {
    return "login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(HttpServletRequest request,
                      @RequestParam String usuario,
                      @RequestParam String password) {
    log.info("[CEDEI-HEROES] login IN - usuario={" + usuario + "}");

    HttpSession session = request.getSession(true);

    session.setAttribute("usuario", usuario);
    session.setAttribute("password", password);

    return "index";
  }

  @RequestMapping(value = "/logout", method = RequestMethod.POST)
  public String logout(HttpServletRequest request) {
    log.info("[CEDEI-HEROES] logout IN");

    HttpSession session = request.getSession(true);

    session.invalidate();

    return "redirect:login";
  }
}
