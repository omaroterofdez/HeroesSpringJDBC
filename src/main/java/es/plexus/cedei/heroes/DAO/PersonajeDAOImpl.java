package es.plexus.cedei.heroes.DAO;

import es.plexus.cedei.heroes.model.Personaje;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by cedei06 on 24/07/2017.
 */
public class PersonajeDAOImpl implements PersonajeDAO{

  private static final Logger logger = LogManager.getLogger(PersonajeDAOImpl.class);

  private SessionFactory sessionFactory;

  public void setSessionFactory(SessionFactory sf) { this.sessionFactory = sf;}

  @Override
  public void addPersonaje(Personaje personaje) {
    Session session = this.sessionFactory.getCurrentSession();
    session.persist(personaje);
    logger.info("Personaje Añadido: " + personaje);
  }

  @Override
  public void updatePersonaje(Personaje personaje) {
    Session session = this.sessionFactory.getCurrentSession();
    session.update(personaje);
    logger.info("Personaje Actualizado: " + personaje);
  }

  @Override
  public void removePersonaje(Long id) {
    Session session = this.sessionFactory.getCurrentSession();
    Personaje personaje = (Personaje) session.load(Personaje.class, new Long(id));
    if (null != personaje) {
      session.delete(personaje);
    }
    logger.info("Personaje borrado: " + personaje);
  }

  @Override
  public List<Personaje> listaPersonajes() {
    Session session = this.sessionFactory.getCurrentSession();
    List<Personaje> personajesLista = session.createQuery("from Personaje").list();
    for (Personaje p : personajesLista) {
      logger.info("Lista Personaje ::" + p);
    }
    return personajesLista;
  }

  @Override
  public Personaje getPersonaje(Long id) {
    Session session = this.sessionFactory.getCurrentSession();
    Personaje personaje = (Personaje) session.load(Personaje.class, new Long(id));
    logger.info("Personaje cargado: " + personaje);
    return personaje;
  }
}
