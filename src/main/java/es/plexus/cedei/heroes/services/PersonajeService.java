package es.plexus.cedei.heroes.services;

import es.plexus.cedei.heroes.model.Personaje;

import java.util.List;

/**
 * Created by cedei06 on 24/07/2017.
 */
public interface PersonajeService {

  public void addPersonaje(Personaje personaje);
  public void updatePersonaje(Personaje personaje);
  public void removePersonaje(Long id);
  public List<Personaje> listaPersonajes();
  public Personaje getPersonaje(Long id);

}
