package es.plexus.cedei.heroes.services;

import es.plexus.cedei.heroes.DAO.PersonajeDAO;
import es.plexus.cedei.heroes.model.Personaje;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by cedei06 on 24/07/2017.
 */
@Service
public class PersonajeServiceImpl implements PersonajeService {

  @Autowired
  private PersonajeDAO personajeDAO;

  public void setPersonajeDAO(PersonajeDAO personajeDAO) {
    this.personajeDAO = personajeDAO;
  }

  public PersonajeServiceImpl(PersonajeDAO personajeDAO) {
    this.personajeDAO = personajeDAO;
  }

  @Override
  @Transactional
  public void addPersonaje(Personaje personaje) {
    this.personajeDAO.addPersonaje(personaje);
  }

  @Override
  @Transactional
  public void updatePersonaje(Personaje personaje) {
    this.personajeDAO.updatePersonaje(personaje);
  }

  @Override
  @Transactional
  public void removePersonaje(Long id) {
    this.personajeDAO.removePersonaje(id);
  }

  @Override
  @Transactional
  public List<Personaje> listaPersonajes() {
    return this.personajeDAO.listaPersonajes();
  }

  @Override
  @Transactional
  public Personaje getPersonaje(Long id) {
    return this.personajeDAO.getPersonaje(id);
  }
}
