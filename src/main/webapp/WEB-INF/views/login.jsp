<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Login</title>
  </head>
  <body>
    <form action="/heroes/login" method="POST">
     <label for="usuario">Usuario:</label>
     <br>
     <input id="usuario" name="usuario" type="text"/>
     <br><br>
     <label for="password">Password:</label>
     <br>
     <input id="password" name="password" type="password">
     <br><br>
     <button type="submit">Login</button>
    </form>
  </body>
</html>